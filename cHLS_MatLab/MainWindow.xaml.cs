﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace cHLS_MatLab
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private C_HLS myC_HLS;

        public MainWindow()
        {
            InitializeComponent();

            myC_HLS = new C_HLS();
        }

        private void Run_cLHS(object sender, RoutedEventArgs e)
        {
            //myC_HLS.cHLS();

            #region simple 3-D example

            // simple 3-D example
            int m = 50; // size of gird

            // Generate 3-D grid size m*m8m
            // [x,y,z]=meshgrid(1:m,1:m,1:50); // TODO implement meshgrid method to produce 3D Grid
            int n = m*m*m;

            // Reshape the grid into single column TODO implement reshape method to return the m-by-n matrix x whose elements are taken column-wise 
            //xc=reshape(x,n,1);   
            //yc=reshape(y,n,1);
            //zc=reshape(z,n,1);
            //x=[xc,yc,zc];


            int nsample = 5;
            int niter = 2000;
            Matrix xobj = new Matrix();
            Matrix vobj = new Matrix();
            int w1 = 1;
            int w2 = 0;
            int w3 = 0;
            //[ipick,xsam,xobs,oL,isam]=cLHS(nsample,x,xobj,vobj,niter,w1,w2,w3); TODO call cLHS method

            //% plot sampled data
            //plot(xsam(:,1), xsam(:,2), 'o');

            #endregion


            #region Soil sampling example

            //load pok05.txt  % input data file
            //% x y CTI Slope ndvi LU
            //data=pok05;

            //coord=data(:,1:2); % site location
            //x=[data(:,3:5)];    % data
            //xobj=data(:,6);     % class data
            //vobj=[1 2 3 4];     % value of the class

            //nsample=100;    % no of samples
            //niter=50000;    % no iterations
            //[ipick,xsam,xobs,oL,isam]=cLHS(nsample,x,xobj,vobj,niter,w1,w2,w3);
            //cxy=coord(ipick,:);
            //% plot sampled data
            //plot(cxy(:,1), cxy(:,2), 'o');

            #endregion

        }
    }
}
