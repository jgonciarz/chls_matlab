﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Media;

namespace cHLS_MatLab
{
    class C_HLS
    {
        public void cHLS(int nsample, object x, ICollection<int> xobj, ICollection<int> vobj,  int niter, int w1, int w2, int w3)
        {
            //MessageBox.Show("Hello");
            /*
             * conditioned Latin hypercube sampling
             * 
             * Input:
             * nsample   = no of sample
             * x = continuous data
             * xobj = class/object data
             * vobj = values of the object
             * 
             * optional:
             * niter = no iterations
             * w1 = weight for continuous data
             * w2 = weight for corelation among data
             * w3 = weight for object data
             * 
             * Output:
             * ipick = index of the sampled picked
             * xsam = sampled continuous data
             * xobs = sampled class data
             * oL = objective function at each iteration
             * isam = matrix containing the count for each strata
             * 
             */

            // parameters
            int nargin = 0; // TODO variable not initialized

            if (nargin == 4)
            {
                niter = 1000;
            }

            if (nargin < 6)
            {
                w1 = 1;
                w2 = 1;
                w3 = 1;
            }

            // annealing schedule
            int temp = 1;
            double tdecrease = 0.9;
            int metrop = 1;

            int ndat = 0;           // my code to instantiate the variable
            // [ndat,nvar]=size(x); TODO implement method size(x) where size(x) returns the number of rows and columns in separate output variables ndat and nvar

            // the edge of the strata
            int step = 100/nsample;
            // Pl=[0:step:100]';     TODO implement an array through for loop from first value 0 through 100 with increments equals to variable step
            // pc=([0.5:ndat]-0.5)./ndat; TODO implement this (-0.5 means subtract 0.5 from each item in the matrix) ? ./ means divide each element of this vector by ndat variable

            for (int j = 1; j <= ndat; j++) // see http://www.mathworks.com/help/matlab/ref/sort.html
            {
                //  xedge(:,j)=prctile(x(:,j),[0:step:100]');   TODO implement this code prctile is percentiles of samples
                // [Y,I] = sort(x(:,j));    TODO implement this sort. x(:,j) returns colums j of matrix x. A good example on sorting http://www.mathworks.com/help/matlab/ref/sort.html
                // xquant(I,j)=pc';         TODO implement this line. pc' is the complex conjugate transpose of pc      
            }

            // target value
            // ibest=ones(nsample,nvar);    TODO create matrix ibest with nsample number of rows and nvar number of columns, populated with integers 1

            // data correlation
            //corr=corrcoef(x);     TODO implement corrcoef(x) method which returns a matrix corr of correlation coefficients calculated from an input matrix X whose rows are observations and whose columns are variables
                                    // See following link for full description of this method http://www.mathworks.com/help/matlab/ref/corrcoef.html

            // for object/class variable
            int nobj;
            int cobj;

            if (xobj.Count == 0)
            {
                nobj = 0;
                cobj = 0;
                w3 = 0;
                //xobs=[]; // TODO use this implementation Matrix xobs = new Matrix();
            }
            else
            {
                nobj=vobj.Count;
                for (int j = 1; j <= nobj; j++)
                {
                    //cobj(j)=sum(xobj==vobj(j));   // TODO create an array which will be populated by result of sum method                   
                }
                 // target value
                // cobj=cobj./ndat*nsample;     // TODO divide each element in the cobj array by ndat (which is a number of rows) multiply by nsample (number of samples)               
            }

            // initialise, pick randomly
            int nunsam=ndat-nsample;
            // ICollection<int> idat = randperm(ndat);  // TODO implement randperm(ndat) returns a row vector containing a random permutation of the integers from 1 to ndat (number of rows) inclusive.    
            // ipick=idat(1:nsample);   // TODO idat(1:nsample) returns vector of values between 1 and nsample
            // iunsam=idat(nsample+1:ndat);    // TODO idat(nsample+1:ndat) returns a vector of values between initial sample + 1 and ndat (number of rows) 
            // xsam=x(ipick,:);    // TODO x(ipick,:) returns sample matrix of data(1 through nsample rows includes all columns) from x (continuous data) 
            if (nobj > 0)
            {
               // xobs=xobj(ipick,:);  // TODO xobj(ipick,:) returns sample matrix of class/object data(1 through nsample rows includes all columns) from xobj (class/object data)    
            }

            // objective function
            //[obj,isam,dif,iobj]=LHSobj(nsample,nvar,xsam,xobs,xedge,ibest,w1,w2,corr,w3,vobj,cobj);   TODO can not locate the implementation of this method

            #region tic toc This function records the internal time at execution of the tic command.

            // tic
            for (int il = 1; il <= niter; il++)
            {
                // il   // TODO displays current iteration
                //idx = randperm(nunsam); // TODO implement randperm(nunsam) returns a row vector containing a random permutation of the integers from 1 to ndat (number of rows - nsample) inclusive.  
                //iunsam   = iunsam(idx); // TODO iunsam matrix resets itself to keep the rows indicated by idx vector ( ) values

                //objsave = obj;    // TODO the obj variable comes as a return object from not implemented method LHSobj above
                //isave = ipick;    // TODO create a vector with vector ipick values
                //iusave = iunsam;  // TODO assign values from vector iunsam to vector iusave
                //dsave = dif;        // TODO the dif variable comes as a return object from not implemented method LHSobj above

                Random rand = new Random();
                if (rand.NextDouble()<0.4)
                {
                    // pick a random sample & resample
                    // iw=randperm(nsample);   // TODO implement randperm(nsample) returns a row vector containing a random permutation of the integers from 1 to nsample inclusive.        
                    // ich=iunsam(1);   // TODO variable ich gets first value from vector iunsam(1)
                    // ipick(iw(1))=ich;   // TODO vector ipick gets value from variable ich at the position set by value iw(1)
                    // iunsam(1)=iw(1);     // TODO iunsam(1) matrix position 1 gets value from iw(1) vector of random nsample number at position 1 
                    // xsam(iw(1),:)=x(ich,:); // TODO xsam matrix sample data gets its rows (determined by first random value of nsample vector) set by original x data matrix rows (determined by value ich which is a random row number of number of rows (original data matrix - sample size nsample)  
                    if (nobj>0)   // nobj is the size of the vobj vector
                    {
                       // xobs(iw(1),:)=xobj(ich,:);  // TODO xobs (sampled class data matrix) gets its rows (determined by the first value of random value of nsmaple) set to xobj (class/object data matrix) rows determined by ich (ich is a first value of random permutation of random permutations of unsampled number or rows of the original data)
                    }
                    else
                    {
                        // remove the worse sampled & resample
                        // worse = max(dif); // TODO max(dif) if dif (is a returned element from not implemented method LHSobj above) is a vector then max returns the largest element; if dif is a matrix then max  treats the columns of dif as vectors, returning a row vector containing the maximum element from each column.
                        // iworse=find(dif==worse);    // TODO implement find(dif==worse) ???
                        //nworse=length(iworse);  // TODO nworse is the number of worse elements in the vector iworse                        
                    }
                    // swap with reservoir
                    //jch=ipick(iworse);      // TODO jch gets the worse case from the randomized sample
                    //kch=iunsam(:,1:nworse);     // TODO kch is a matrix of all rows and columns from 1 to the number of worse finds
                    ipick(iworse)=kch;
                    iunsam(:,1:nworse)=jch;    
                    xsam(iworse,:)=x(kch,:);
                    if(nobj>0), xobs(iworse,:)=xobj(kch,:); end;
                   
                }
            }
            // toc

            #endregion
        }
    }
}
